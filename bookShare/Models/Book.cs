﻿using System;
using Microsoft.AspNetCore.Identity;

namespace bookShare.Models
{
    public class Book
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Author { get; set; }

        public string? UserId { get; set; }
        public virtual IdentityUser? User { get; set; }

        public bool Available { get; set; }
        public string? BorrowerId { get; set; }
    }
}