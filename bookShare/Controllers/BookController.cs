using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using bookShare.Data;
using bookShare.Models;
using Microsoft.AspNetCore.Identity;

namespace bookShare.Controllers
{
    public class BookController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public BookController(ApplicationDbContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Book
        public async Task<IActionResult> Index(string? query)
        {

            Console.WriteLine("here is query");
            Console.WriteLine(query);
            var applicationDbContext = _context.Book.Include(b => b.User).Where(b => b.UserId != _userManager.GetUserId(User) & b.Available == true & b.BorrowerId == null);
            if (!String.IsNullOrEmpty(query))
            {
                applicationDbContext = applicationDbContext.Where(b => b.Name.Contains(query) || b.Author.Contains(query));
            }
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: MyBooks
        public async Task<IActionResult> OwnIndex()
        {
            if (_userManager.GetUserId(User) == null)
                return RedirectToAction(nameof(Index));
            var applicationDbContext = _context.Book.Include(b => b.User).Where(b => b.UserId == _userManager.GetUserId(User));
            foreach(var book in applicationDbContext)
            {
                if(book.BorrowerId != null)
                {
                    string user = _userManager.FindByIdAsync(book.BorrowerId).Result.UserName;
                    ViewData[book.BorrowerId] = user;
                }
            }
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Book/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (_userManager.GetUserId(User) == null)
                return RedirectToAction(nameof(Index));
            if (id == null || _context.Book == null)
            {
                return NotFound();
            }

            var book = await _context.Book
                .Include(b => b.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        // GET: Book/Create
        public IActionResult Create()
        {
            if (_userManager.GetUserId(User) == null)
                return RedirectToAction(nameof(Index));
            return View();
        }

        // GET: Book/Requests
        public async Task<IActionResult> BorrowRequests()
        {
            if (_userManager.GetUserId(User) == null)
                return RedirectToAction(nameof(Index));
            var applicationDbContext = _context.Book.Include(b => b.User).Where(b => b.UserId == _userManager.GetUserId(User) & b.BorrowerId != null & b.Available == true);
            foreach (var book in applicationDbContext)
            {
                string user = _userManager.FindByIdAsync(book.BorrowerId).Result.UserName;
                ViewData[book.BorrowerId] = user;
            }
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Book/MyRequests
        public async Task<IActionResult> MyRequests()
        {
            if (_userManager.GetUserId(User) == null)
                return RedirectToAction(nameof(Index));
            var applicationDbContext = _context.Book.Include(b => b.User).Where(b => b.BorrowerId == _userManager.GetUserId(User) & b.Available == true);
            foreach (var book in applicationDbContext)
            {
                string user = _userManager.FindByIdAsync(book.BorrowerId).Result.UserName;
                ViewData[book.BorrowerId] = user;
            }
                
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Book/BorrowedBooks
        public async Task<IActionResult> BorrowedBooks()
        {
            if (_userManager.GetUserId(User) == null)
                return RedirectToAction(nameof(Index));
            var applicationDbContext = _context.Book.Include(b => b.User).Where(b => b.UserId != _userManager.GetUserId(User) & b.Available == false & b.BorrowerId != null);
            foreach (var book in applicationDbContext)
            {
                string user = _userManager.FindByIdAsync(book.BorrowerId).Result.UserName;
                ViewData[book.BorrowerId] = user;
            }

            return View(await applicationDbContext.ToListAsync());
        }

        // POST: Book/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Author,Available")] Book book)
        {
            if (_userManager.GetUserId(User) == null)
                return RedirectToAction(nameof(Index));
            book.UserId = _userManager.GetUserId(User);
            if (ModelState.IsValid)
            {
                _context.Add(book);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(OwnIndex));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", book.UserId);
            return View(book);
        }

        // GET: Book/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (_userManager.GetUserId(User) == null)
                return RedirectToAction(nameof(Index));
            if (id == null || _context.Book == null)
            {
                return NotFound();
            }

            var book = await _context.Book.FindAsync(id);
            if (book == null)
            {
                return NotFound();
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", book.UserId);
            return View(book);
        }

        // POST: Book/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Author,UserId,Available,BorrowerId")] Book book)
        {
            if (_userManager.GetUserId(User) == null)
                return RedirectToAction(nameof(Index));
            if (id != book.Id)
            {
                return NotFound();
            }

            book.UserId = _userManager.GetUserId(User);

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(book);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookExists(book.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(OwnIndex));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", book.UserId);
            return View(book);
        }

        // GET: Book/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (_userManager.GetUserId(User) == null)
                return RedirectToAction(nameof(Index));
            if (id == null || _context.Book == null)
            {
                return NotFound();
            }

            var book = await _context.Book
                .Include(b => b.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        // POST: Book/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_userManager.GetUserId(User) == null)
                return RedirectToAction(nameof(Index));
            if (_context.Book == null)
            {
                return Problem("Entity set 'ApplicationDbContext.Book'  is null.");
            }
            var book = await _context.Book.FindAsync(id);
            if (book != null)
            {
                _context.Book.Remove(book);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(OwnIndex));
        }

        // POST: Book/AskBorrow/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AskBorrow(int id)
        {
            if (_userManager.GetUserId(User) == null)
                return RedirectToAction(nameof(Index));
            if (_context.Book == null)
            {
                return Problem("Entity set 'ApplicationDbContext.Book'  is null.");
            }

            var book = await _context.Book.FindAsync(id);

            if (id != book.Id)
            {
                return NotFound();
            }

            book.BorrowerId = _userManager.GetUserId(User);

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(book);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookExists(book.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", book.UserId);
            return View(book);
        }

        // GET: Book/RejectBorrow/5
        public async Task<IActionResult> RejectBorrow(int? id)
        {
            if (_userManager.GetUserId(User) == null)
                return RedirectToAction(nameof(Index));
            if (id == null || _context.Book == null)
            {
                return NotFound();
            }

            var book = await _context.Book
                .Include(b => b.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            ViewData[book.BorrowerId] = _userManager.FindByIdAsync(book.BorrowerId).Result.UserName;
            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        // POST: Book/RejectBorrow/5
        [HttpPost, ActionName("RejectBorrow")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RejectBorrowConfirm(int id)
        {
            if (_userManager.GetUserId(User) == null)
                return RedirectToAction(nameof(Index));
            var book = await _context.Book.FindAsync(id);

            if (id != book.Id)
            {
                return NotFound();
            }

            book.BorrowerId = null;
            book.Available = true;

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(book);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookExists(book.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", book.UserId);
            return View(book);
        }

        // GET: Book/AcceptBorrow/5
        public async Task<IActionResult> AcceptBorrow(int? id)
        {
            if (_userManager.GetUserId(User) == null)
                return RedirectToAction(nameof(Index));
            if (id == null || _context.Book == null)
            {
                return NotFound();
            }

            var book = await _context.Book
                .Include(b => b.User)
                .FirstOrDefaultAsync(m => m.Id == id);

            ViewData[book.BorrowerId] = _userManager.FindByIdAsync(book.BorrowerId).Result.UserName;

            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        // POST: Book/AcceptBorrow/5
        [HttpPost, ActionName("AcceptBorrow")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AcceptBorrowConfirm(int id)
        {
            if (_userManager.GetUserId(User) == null)
                return RedirectToAction(nameof(Index));
            var book = await _context.Book.FindAsync(id);

            if (id != book.Id)
            {
                return NotFound();
            }

            book.Available = false;

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(book);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookExists(book.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", book.UserId);
            return View(book);
        }

        // GET: Book/Return/5
        public async Task<IActionResult> Return(int? id)
        {
            if (_userManager.GetUserId(User) == null)
                return RedirectToAction(nameof(Index));
            if (id == null || _context.Book == null)
            {
                return NotFound();
            }

            var book = await _context.Book
                .Include(b => b.User)
                .FirstOrDefaultAsync(m => m.Id == id);

            ViewData[book.BorrowerId] = _userManager.FindByIdAsync(book.BorrowerId).Result.UserName;

            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        // POST: Book/AcceptBorrow/5
        [HttpPost, ActionName("Return")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ReturnConfirm(int id)
        {
            if (_userManager.GetUserId(User) == null)
                return RedirectToAction(nameof(Index));
            var book = await _context.Book.FindAsync(id);

            if (id != book.Id)
            {
                return NotFound();
            }

            book.Available = true;
            book.BorrowerId = null;

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(book);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookExists(book.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", book.UserId);
            return View(book);
        }

        private bool BookExists(int id)
        {
            return (_context.Book?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
